# README #

## What is this? ##

This is a quantum computer simulator built in Java. Currently, it is based off of the [D-wave](https://www.dwavesys.com/) quantum computer. They have open sourced the [compiler](https://github.com/dwavesystems/qbsolv). This simulator is runs off of code in the same format as what goes into the compiler, in an attempt to make the transition from simulator to physical quantum computer as seemless as possible.

## Java seems slow for this... ##

Correct! However, I felt it would be the easiest to get started, and work on any computer. After the Java version is polished, development would focus on C++ and CUDA support to accelerate processing.

## Won't it take forever to simulate? ##

Correct! It may also run your computer out of RAM... But the intention is to allow programs to be developed without the constant need to test on a physical quantum computer.
