package org.coinofgold.quantumsimulator;

import java.util.regex.Pattern;

public class Global {
	
	// Duplicate checking
	public final static int QUBIT_EXACT_DUPLICATE = 0;
	public final static int QUBIT_SAME_INDEX = 1;
	public final static int QUBIT_OKAY = 2;
	public final static int COUPLER_EXACT_DUPLICATE = 0;
	public final static int COUPLER_SAME_QUBITS = 1;
	public final static int COUPLER_OKAY = 2;

	// Topology
	public final static String UNCONSTRAINED1 = "0";
	public final static String UNCONSTRAINED2 = "unconstrained";
	public final static String CHIMERA128 = "chimera128"; // Not supported
	public final static String CHIMERA512 = "chimera512"; // Not supported
	
	// QUBO input indicators
	public final static String PROBLEM_LINE = "p";
	public final static String COMMENT_LINE = "c";
	public final static String SEPARATOR = " ";
	public final static int REQUIRED_PROBLEM_ARGUMENTS = 6;
	public final static int REQUIRED_VALUE_ARGUMENTS = 3;
	
	// QUBO problem line
	public final static String QUBO_FILE_TYPE = "qubo";
	
}
