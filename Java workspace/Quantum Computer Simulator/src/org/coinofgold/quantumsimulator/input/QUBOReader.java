package org.coinofgold.quantumsimulator.input;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.coinofgold.quantumsimulator.Global;
import org.coinofgold.quantumsimulator.qpu.Qubit;
import org.coinofgold.quantumsimulator.qpu.Coupler;

public class QUBOReader {

	private boolean problemLineRead;
	
	private int maxQubits;
	private int nQubits;
	private int nCouplers;
	private ArrayList<Qubit> qubits;
	private ArrayList<Coupler> couplers;
	
	public QUBOReader() {
		problemLineRead = false;
		qubits = new ArrayList<Qubit>();
		couplers = new ArrayList<Coupler>();
	}
	
	public boolean readFile(String filename, boolean displayComments, boolean displayFile) {
		problemLineRead = false;
		
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			for (String line; (line = br.readLine()) != null; ) {
				if (line.equals("")) {
					// Blank line
				} else if (line.startsWith(Global.COMMENT_LINE)) {
					// Comment line
					if (displayComments) {
						String comment = line.substring(Global.COMMENT_LINE.length());
						System.out.println(comment);
					}
				} else if (line.startsWith(Global.PROBLEM_LINE)) {
					// Problem definition line
					if (!parseProblemLine(line)) {
						return false;
					}
				} else {
					if (!parseValueLine(line)) {
						return false;
					}
				}
				
				if (displayFile) {
					System.out.println(line);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private boolean parseProblemLine(String line) {
		String[] parts = line.split(Global.SEPARATOR);
		if (parts.length == Global.REQUIRED_PROBLEM_ARGUMENTS) {
			// File type
			if (parts[1].equals(Global.QUBO_FILE_TYPE)) {
				// Good
			} else {
				System.err.println("Not a supported file type:");
				System.err.println(parts[1]);
				return false;
			}
			
			// Topology
			if (parts[2].equals(Global.UNCONSTRAINED1) || parts[2].equals(Global.UNCONSTRAINED2)) {
				// Good
			} else {
				System.err.println("Not a supported topology:");
				System.err.println(parts[2]);
				return false;
			}
			
			// Max nodes
			try {
				int max = Integer.parseInt(parts[3]);
				if (max > 0) {
					maxQubits = max;
					qubits = new ArrayList<Qubit>(maxQubits);
				} else {
					System.err.println("Invalid maximum number of qubits:");
					System.err.println(max);
					return false;
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				return false;
			}
			
			// Number of nodes
			try {
				int nodes = Integer.parseInt(parts[4]);
				if ((nodes > 0) && (nodes <= maxQubits)) {
					nQubits = nodes;
				} else {
					System.err.println("Invalid number of qubits:");
					System.err.println(nodes);
					return false;
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				return false;
			}
			
			// Number of couplers
			try {
				int couplers = Integer.parseInt(parts[5]);
				if ((couplers >= 0) && (couplers <= nQubits * nQubits)) {
					nCouplers = couplers;
				} else {
					System.err.println("Invalid number of qubits:");
					System.err.println(couplers);
					return false;
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				return false;
			}
		} else {
			System.err.println("Invalid line, must have "+ Global.REQUIRED_PROBLEM_ARGUMENTS +" arguments:");
			System.err.println(line);
			return false;
		}
		
		problemLineRead = true;
		return true;
	}
	
	private boolean parseValueLine(String line) {
		if (!problemLineRead) {
			System.err.println("Encountered line before problem line:");
			System.err.println(line);
			return false;
		}
		
		String[] parts = line.split(Global.SEPARATOR);
		if (parts.length == Global.REQUIRED_VALUE_ARGUMENTS) {
			try {
				int first = Integer.parseInt(parts[0]);
				int second = Integer.parseInt(parts[1]);
				float value = Float.parseFloat(parts[2]);
				
				try {
					if (first == second) {
						// Weight
						Qubit q = new Qubit(first, value);
						int result = doesQubitAlreadyExist(q);
						if (result == Global.QUBIT_OKAY) {
							qubits.add(q);
						} else if (result == Global.QUBIT_SAME_INDEX) {
							return false;
						} else if (result == Global.QUBIT_SAME_INDEX) {
							// Ignored
						}
					} else if (first < second) {
						// Coupler
						Coupler c = new Coupler(first, second, value);
						int result = doesCouplerAlreadyExist(c);
						if (result == Global.COUPLER_OKAY) {
							couplers.add(c);
						} else if (result == Global.COUPLER_SAME_QUBITS) {
							return false;
						} else if (result == Global.COUPLER_EXACT_DUPLICATE) {
							// Ignored
						}
					} else {
						System.err.println("Invalid line, must have firstIndex < secondIndex:");
						System.err.println(line);
						return false;
					}
				} catch (ArrayIndexOutOfBoundsException aioobe) {
					aioobe.printStackTrace();
				}
			} catch (NumberFormatException nfe) {
				nfe.printStackTrace();
				return false;
			}
		} else {
			System.err.println("ERROR: Invalid line, must have "+ Global.REQUIRED_VALUE_ARGUMENTS +" arguments:");
			System.err.println(line);
			return false;
		}
		
		return true;
	}
	
	private int doesQubitAlreadyExist(Qubit q) {
		for (Qubit qubit: qubits) {
			if (q.isSameIndex(qubit)) {
				if (q.equals(qubit)) {
					System.out.println("WARNING: Exact duplicate qubit found, ignoring.");
					return Global.QUBIT_EXACT_DUPLICATE;
				} else {
					System.err.println("ERROR: Found qubit with different weight.");
					return Global.QUBIT_SAME_INDEX;
				}
			}
		}
		
		return Global.QUBIT_OKAY;
	}
	
	private int doesCouplerAlreadyExist(Coupler c) {
		for (Coupler coupler: couplers) {
			if (c.isSameQubits(coupler)) {
				if (c.equals(coupler)) {
					System.out.println("WARNING: Exact duplicate coupler found, ignoring.");
					return Global.COUPLER_EXACT_DUPLICATE;
				} else {
					System.err.print("ERROR: Found coupler with different strength.");
					return Global.COUPLER_SAME_QUBITS;
				}
			}
		}
		
		return Global.COUPLER_OKAY;
	}
	
	public int getMaxQubits() {
		return maxQubits;
	}
	
	public int getNumberOfQubits() {
		return nQubits;
	}
	
	public int getNumberOfCouplers() {
		return nCouplers;
	}
	
	public Qubit[] getQubits() {
		return qubits.toArray(new Qubit[qubits.size()]);
	}
	
	public Coupler[] getCouplers() {
		return couplers.toArray(new Coupler[couplers.size()]);
	}
	
}
