package org.coinofgold.quantumsimulator;

import org.coinofgold.quantumsimulator.input.QUBOReader;
import org.coinofgold.quantumsimulator.qpu.QPU;

public class QuantumComputerSimulator {

	public static void main(String[] args) {
		new QuantumComputerSimulator(false);
	}
	
	public QuantumComputerSimulator(boolean gui) {
		QUBOReader reader = new QUBOReader();
		boolean read = reader.readFile("benchmark.qubo", true, false);
		System.out.println("Read: "+ read);
		QPU qpu = new QPU(reader.getMaxQubits(), reader.getNumberOfQubits(), reader.getNumberOfCouplers(), reader.getQubits(), reader.getCouplers(), true, 0);
		boolean success = qpu.process();
		System.out.println("Finished: "+ success);
	}

}
