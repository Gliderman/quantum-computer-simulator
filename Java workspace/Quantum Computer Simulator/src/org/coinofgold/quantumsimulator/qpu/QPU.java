package org.coinofgold.quantumsimulator.qpu;

import java.util.Arrays;

public class QPU {
	
	// Static data
	private final int maxQubits;
	private final int nQubits;
	private final int nCouplers;
	private final Qubit[] qubits;
	private final Coupler[] couplers;
	
	// Configuration data
	private final boolean minimize;
	private final int nSolutions;
	
	// Live data
	private float currentObjective;
	private float[] bestObjectives;
	private byte qubitValues;

	public QPU(int maxQubits, int nQubits, int nCouplers,
			Qubit[] qubits, Coupler[] couplers,
			boolean minimize, int nSolutions) {
		this.maxQubits = maxQubits;
		this.nQubits = nQubits;
		this.nCouplers = nCouplers;
		this.qubits = qubits;
		this.couplers = couplers;
		
		this.minimize = minimize;
		this.nSolutions = nQubits*nQubits;
		
		resetLiveData();
	}
	
	public boolean process() {
		resetLiveData();
		
		do {
			boolean qubitSuccess = processQubits();
			if (!qubitSuccess) {
				return false;
			}

			boolean couplerSuccess = processCouplers();
			if (!couplerSuccess) {
				return false;
			}
			
			bestObjectives[qubitValues] = currentObjective;
			String state = String.format("%8s", Integer.toBinaryString(qubitValues & 0xFF)).replace(' ', '0');
			System.out.println(state +" "+ currentObjective);

			configureForNextIteration();
		} while (!done());
		
		Arrays.sort(bestObjectives);
		System.out.println("Objectives:");
		for (float objective: bestObjectives) {
			System.out.println(objective);
		}
		
		return true;
	}
	
	private boolean processQubits() {
		for (Qubit q: qubits) {
			if (getQubitValue(q.index) != 0) {
				currentObjective += q.weight;
			} else {
				// Do nothing
			}
		}
		
		return true;
	}
	
	private boolean processCouplers() {
		for (Coupler c: couplers) {
			if ((getQubitValue(c.qubiti) != 0) && (getQubitValue(c.qubitj) != 0)) {
				currentObjective += c.strength;
			} else {
				// Do nothing
			}
		}
		
		return true;
	}
	
	private void configureForNextIteration() {
		currentObjective = 0.0f;
		qubitValues += 1;
	}
	
	private int getQubitValue(int index) {
		return (qubitValues >> index) & 1;
	}
	
	private boolean done() {
		if (qubitValues >= nQubits * nQubits) {
			return true;
		} else {
			return false;
		}
	}
	
	private void resetLiveData() {
		currentObjective = 0.0f;
		bestObjectives = new float[nSolutions];
		qubitValues = 0;
	}
	
}
