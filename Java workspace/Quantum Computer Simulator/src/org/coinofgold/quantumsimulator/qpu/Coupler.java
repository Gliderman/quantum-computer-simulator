package org.coinofgold.quantumsimulator.qpu;

public class Coupler {

	public final int qubiti; // i < j
	public final int qubitj;
	public final float strength;
	
	public Coupler(int qubiti, int qubitj, float strength) {
		this.qubiti = qubiti;
		this.qubitj = qubitj;
		this.strength = strength;
	}
	
	public boolean isSameQubits(Coupler c) {
		if ((qubiti == c.qubiti) && (qubitj == c.qubitj)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Coupler c) {
		if ((qubiti == c.qubiti) && (qubitj == c.qubitj) && (strength == c.strength)) {
			return true;
		} else {
			return false;
		}
	}
	
}
