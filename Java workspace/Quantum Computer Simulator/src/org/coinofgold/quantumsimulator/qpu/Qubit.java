package org.coinofgold.quantumsimulator.qpu;

public class Qubit {

	public final int index;
	public final float weight;
	
	public Qubit(int index, float weight) {
		this.index = index;
		this.weight = weight;
	}
	
	public boolean isSameIndex(Qubit q) {
		if (index == q.index) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Qubit q) {
		if ((index == q.index) && (weight == q.weight)) {
			return true;
		} else {
			return false;
		}
	}
	
}
